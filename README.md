# burger with sequelize

### Summary
The burger app logs burgers added via a form on the page and creates a button, which when clicked then "devours" the burger, moving it to the devoured section. This is the same app as "burger" but uses Sequelize in place of a custom ORM and SQL queries.

### Tools used
Model-View-Controller design pattern <br>
**Model/Controller:** Node, Express, MySQL, Sequelize <br>
**View:** Handlebars, Materialize, CSS, jQuery, Google Fonts <br>

### How to use
Navigate to the app's [homepage](https://immense-springs-64181.herokuapp.com/).

![Burger homepage](./images/main.png)

Add a customer's name using the form:

<img src="./images/add-customer.png" alt="add customer" title="add customer" width="400" />

Add a burger:

<img src="./images/add-burger.png" alt="add burger" title="add burger" width="400" />

Click submit and a button will be created for your burger.

<img src="./images/buttons.png" alt="Burger added" title="Burger added" width="400" />

Click on any of the burger buttons to devour that burger. The name of the customer who submitted that burger will be printed alongside it.

<img src="./images/devoured.png" alt="Burger devoured" title="Burger devoured" width="400" />