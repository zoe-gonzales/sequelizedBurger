var express = require('express');
var db = require('../models');
var router = express.Router();

// get all burgers from db
router.get('/', function(req, res){
    db.Customer.findAll({include: [ db.Burger ]}).then(function(data){
        res.render('index', {customers: data});
    });
});

// add one burger to db
router.post('/api/burgers', function(req, res){
    db.Burger.create(req.body).then(function(burgerData){
        res.json(burgerData);
    });
});

// add customers to db
router.post('/api/customers', function(req, res){
    db.Customer.create(req.body).then(function(customerData){
        res.json(customerData);
    });
});

// update devoured status of burger
router.put('/api/burgers/:id', function(req, res){
    db.Burger.update(
        {
            devoured: req.body.devoured
        },
        {
            where: {
                id: req.params.id
            }
        }
    ).then(function(burgerData){
        res.json(burgerData);
    });
});

module.exports = router;