$(document).ready(function(){
    $('select').formSelect();    
    
    $('#add-customer').on('submit', function(e){
        e.preventDefault();
        var newCustomer = {
            name: $('#add-customer [name=newCustomer]').val().trim()
        }

        $.ajax('/api/customers', {
            method: 'POST',
            data: newCustomer
        }).then(function(){
            location.reload();
        });

    });

    $('#add-burger').on('submit', function(e){
        e.preventDefault();
        
        var newBurger = {
            burger_name: $('#add-burger [name=newBurger]').val().trim(),
            devoured: false,
            CustomerId: $('#customerId').val()
        }
        
        $.ajax('/api/burgers', {
            method: 'POST',
            data: newBurger
        }).then(function(){
            location.reload();
        });
    });

    $('.undevoured').on('click', function(){
        var id = $(this).data('id');

        var updatedBurger = {
            devoured: 1
        }

        $.ajax('/api/burgers/' + id, {
            method: 'PUT',
            data: updatedBurger
        }).then(function(){
            location.reload();
        });
    })
});